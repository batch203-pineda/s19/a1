// console.log("Hello World!");


let username;
let password;
let role;



function logInFunction() {
    username = prompt("Enter Username");
    password = prompt("Enter Password");
    role = prompt("Enter Role");
    
    if ( username === "" || username === null ||
        password === "" || password === null ||
        role === "" || role === null
        ) {
            alert("Input must not be empty, try again");
            logInFunction();
        }
        else {
            switch (role){
                        case "admin" :
                            alert("Welcome back to the class portal, " + role+ "!");
                            break;
                        case "teacher" :
                            alert("Thank you for loggin in, " + role + "!");
                            break;
                        case "student" :
                            alert("Welcome to the class portal, " + role + "!");
                            break;
                        default :
                            alert("Role out of range");
                            logInFunction();
                    }
        }
}

logInFunction();


function checkAverageNumber(num1, num2, num3, num4) {
    
    let sum = num1+num2+num3+num4;
    let avg = sum/4;


     if (Math.round(avg) <= 74){
        console.log("Check Average(" + num1 + "," + num2 + "," + num3 + "," + num4 + ")");
        console.log("Hello student, your average is " + Math.round(avg) + ".The letter equivalent is F");
    } else if (Math.round(avg) >= 75 && Math.round(avg) <= 79){
        console.log("Check Average(" + num1 + "," + num2 + "," + num3 + "," + num4 + ")");
        console.log("Hello student, your average is " + Math.round(avg) + ".The letter equivalent is D");
    }  else if (Math.round(avg) >= 80 && Math.round(avg) <= 84){
        console.log("Check Average(" + num1 + "," + num2 + "," + num3 + "," + num4 + ")");
        console.log("Hello student, your average is " + Math.round(avg) + ".The letter equivalent is C");
    }  else if (Math.round(avg) >= 85 && Math.round(avg) <= 89){
        console.log("Check Average(" + num1 + "," + num2 + "," + num3 + "," + num4 + ")");
        console.log("Hello student, your average is " + Math.round(avg) + ".The letter equivalent is B");
    }  else if (Math.round(avg) >= 90 && Math.round(avg) <= 95){
        console.log("Check Average(" + num1 + "," + num2 + "," + num3 + "," + num4 + ")");
        console.log("Hello student, your average is " + Math.round(avg) + ".The letter equivalent is A");
    }  else if (Math.round(avg) >= 96){
        console.log("Check Average(" + num1 + "," + num2 + "," + num3 + "," + num4 + ")");
        console.log("Hello student, your average is " + Math.round(avg) + ".The letter equivalent is A+");
    } 
    
}

checkAverageNumber(71,70,73,74);
checkAverageNumber(75,75,76,78);
checkAverageNumber(80,81,82,78);
checkAverageNumber(84,85,87,88);
checkAverageNumber(89,90,91,90);
checkAverageNumber(91,96,97,95);
checkAverageNumber(91,96,97,99);